---
title: Apa Itu JWT (JSON Web Token)?
image: "jwt-logo.webp"
url: /apa-itu-jwt
aliases:
  - /apa-itu-jwt/amp/
description: Pada tutorial ini kita akan mengenal tentang apa itu JWT dan juga belajar bagaimana cara kerja dari JWT
date: 2022-06-07
author: "Ruang Koding"
lastmod: 2022-06-22
tags: ["Node.js"]
categories: ["Pemrograman"]
draft: false
schema:
  image:
    url: "jwt-logo.webp"
  author: "Ruang Koding"
  keywords: ["jwt","apa itu jwt", "jwt adalah", "cara kerja jwt"]
---

JWT adalah singkatan dari JSON Web Token yaitu sebuah JSON Object yang digunakan untuk aktifitas transfer informasi antar platform. JWT dapat digunakan untuk sistem otentikasi dan juga dapat digunakan untuk pertukaran informasi. JWT terdiri dari header, payload dan signature. Ketiga bagian ini dipisahkan oleh tanda titik -titik (.).

## Bagaimana Cara Kerja JWT ?

Cara kerja JWT seperti ini : Server akan melakukan generate token yang mensertifikasi identitas user, dan mengirimkannya ke klien. Klien akan mengirim token kembali ke server untuk setiap request ke API, sehingga Server side akan bisa mengetahui asal dari request tersebut.

![Cara Kerja JWT](cara-kerja-jwt.webp)

## Bagaimana token JWT dihasilkan?

Menggunakan Node.js Anda dapat menghasilkan bagian `header` dari token dengan menggunakan kode ini:

```javascript
const header = { "alg": "HS256", "typ": "JWT" }
const encodedHeader = Buffer.from(JSON.stringify(header)).toString('base64')
```

Algoritma yang digunakan untuk signature adalah **HMACSHA256** (JWT mendukung beberapa algoritma), kita akan membuat buffer dari object JSOB lalu kita enkripsikan hasilnya dengan menggunakan Base64.

Hasil dari kode diatas adalah `eyjhbgcioijiuzi1niisinr5cci6ikpxvcj9`.

Selanjutnya kita menambahkan payload, yang dapat kita sesuaikan dengan jenis data apapun. Anda dapat menambahkan data Anda sendiri ke dalam token dengan menggunakan contoh objek berikut ini:

```javascript
const payload = { username: 'RuangKoding' }
```

Selanjutnya kita membuat buffer dari object payload diatas yang sudah kita encode menjadi JSON object dan di enkripsikan menggunakan Base64 seperti yang kita lakukan sebelumnya pada bagian header:

```javascript
const encodedPayload = Buffer.from(JSON.stringify(payload)).toString('base64')
```

Hasil dari kode diatas adalah `eyj1c2vybmftzsi6ikzsyxzpbyj9`.

Setelah selesai membuat header dan payload, kita harus memastikan bahwa isi token tidak dapat diubah. Untuk melakukan ini, kita harus membuat signature dengan menggunakan modul Node Crypto:

```javascript
const crypto = require('crypto')
const jwtSecret = 'secretKey'
const signature = crypto.createHmac('sha256', jwtSecret)
                        .update(encodedHeader + '.' + encodedPayload)
                        .digest('base64')
```

Kita menggunakan secret key rahasia lalu yang enkripsikan dengan menggunakan Base64 dari signature terenkripsi.  

Nilai tanda tangan dalam kasus kami adalah `Mqwecywut7bayj8mivgsj8kdyi3zrvs+wrrzjfzrgrw=`

Sekarang, kita hanya perlu menggabungkan 3 bagian token yaitu `header`, `payload`, dan `signature` dengan memisahkannya dengan tanda titik (.) seperti pada kode berikut :

```javascript
const jwt = `${encodedHeader}.${encodedPayload}.${signature}`
```

Maka, hasilnya adalah sebagai berikut `eyjhbgcioijiuzi1niisinr5cci6ikpxvcj9.eyj1c2vybmftzsi6ikzsyxzpbyj9.Mqwecywut7bayj8mivgsj8kdyi3zrvs+wrrzjfzrgrw=`.

## Apakah JWT aman?

Penting untuk dicatat bahwa JWT menjamin kepemilikan data tetapi tidak dengan keamanan enkripsi. Data JSON yang Anda simpan ke JWT dapat dilihat oleh siapa pun yang mencegat token karena data token tersebut hanya berbentuk serialized dan tidak dienkripsi.

Untuk alasan ini, sangat disarankan untuk menggunakan HTTPS dengan JWTS jika Anda berencana untu menggunakan JWT untuk session token dalam keperluan otentikasi.
