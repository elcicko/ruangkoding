---
title: Tutorial Cara Install Node.Js di Ubuntu 20.04
image: "nodejs-logo.webp"
url: /tutorial-cara-install-nodejs-di-ubuntu-20-04
aliases:
  - /tutorial-cara-install-nodejs-di-ubuntu-20-04/amp/
description: Pada tutorial ini kita akan belajar 3 cara berbeda untuk menginstall Node.js di Ubuntu 20.04.
date: 2022-06-13
author: "Ruang Koding"
lastmod: 2022-06-22
tags: ["Node.js"]
categories: ["Linux"]
draft: false
schema:
  image:
    url: "nodejs-logo.webp"
  author: "Ruang Koding"
  keywords: ["nodejs adalah","cara install nodejs di ubuntu"]
---

Node.js adalah program runtime lintas platform open source untuk menjalankan kode JavaScript di lingkungan Server Side. Node.js menggunakan Node Package Manager (npm) sebagai software packages resminya. Node.js digunakan untuk mengembangkan aplikasi baik di lingkungan front-end maupun back-end. Dengan komunitas kontributor yang besar dan dokumentasi yang baik, Node.js adalah tools yang sangat diminati oleh para developer.

Pada tutorial ini kita akan belajar 3 cara berbeda untuk menginstall Node.js di Ubuntu 20.04.

## Cara Install Node.js di Ubuntu 20.04

Ada 3 cara yang bisa kamu gunakan untuk menginstall Node.js di Ubuntu 20.04, diantaranya adalah :

- Dari repositori standar Ubuntu. Ini adalah cara termudah untuk menginstal Node.js di Ubuntu dan seharusnya cukup untuk sebagian besar kasus penggunaan. Versi yang disertakan dalam repositori Ubuntu adalah 10.19.0.
- Dari repositori NodeSource. Gunakan repositori ini jika Anda ingin menginstal versi Node.js yang berbeda dari yang disediakan di repositori Ubuntu. Saat ini, NodeSource mendukung Node.js v14.x, v13.x, v12.x, dan v10.x.
- Menggunakan nvm (Node Version Manager). Tools ini memungkinkan Anda untuk menginstal beberapa versi Node.js pada mesin yang sama. Jika Anda adalah developer Node.js, maka ini adalah cara yang lebih disukai untuk menginstal Node.js.

### Instal Node.js repositori Ubuntu

Proses Instalasi dengan cara ini cukup mudah. Jalankan perintah berikut untuk memperbarui package dan menginstal Node.js di Ubuntu 20.04 :

```console
sudo apt update
sudo apt install nodejs npm
```

Perintah di atas akan menginstal sejumlah package, termasuk tools yang diperlukan untuk mengcompile dan menginstal add-on dari npm.

Setelah proses instalasi selesai, ketikkan perintah berikut untuk memeriksa versi dari Node.js yang sudah kita install

```console
node --version
```

```console
Output: 
v16.13.2

```

### Menginstal Node.js dan npm dari NodeSource

[NodeSource](https://www.nodesource.com/) adalah perusahaan yang berfokus pada penyediaan dukungan Node tingkat perusahaan. NodeSource memelihara repositori APT yang berisi beberapa versi Node.js. Gunakan repositori ini jika aplikasi Anda memerlukan versi Node.js tertentu.

Pada saat penulisan artikel, repositori NodeSource menyediakan versi berikut:

- v14.x - Versi stabil terbaru.
- v13.x
- v12.x - Versi LTS terbaru.
- v10.x - Versi LTS sebelumnya.

Kita akan menginstal Node.js versi 14.x:

Jalankan perintah berikut sebagai user dengan level root untuk mengunduh dan menjalankan script instalasi NodeSource:

```console
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
```

Script akan menambahkan signature key NodeSource ke sistem Anda, membuat file repositori apt, menginstal semua paket yang diperlukan, dan memperbaharui cache apt. Jika Anda membutuhkan versi Node.js yang lain, misalnya 12.x, ubah setup_14.x dengan setup_12.x.

Setelah repositori NodeSource diaktifkan, instal Node.js dan npm:

```console
sudo apt install nodejs
```

Package nodejs sudah berisi node dan npm.

Kita tinggal melakukan verifikasi bahwa Node.js dan npm berhasil diinstal dengan perintah berikut:

```console
node --version
```

```console
Output:
v14.2.0
```

```console
npm --version
```

```console
Output:
6.14.4
```

Untuk dapat mengcompile add-on dari npm, Anda harus menginstal development tools di Ubuntu:

```console
sudo apt install build-essential
```

### Menginstal Node.js dan npm menggunakan NVM

[NVM (Node Version Manager)](https://github.com/nvm-sh/nvm#installing-and-updating) adalah bash script yang memungkinkan Anda mengelola beberapa versi Node.js per user. Dengan NVM Anda dapat menginstal dan menghapus versi Node.js apa pun yang ingin Anda gunakan atau uji.

Kunjungi halaman repositori nvm GitHub dan salin perintah curl atau wget untuk mengunduh dan menginstal script nvm:

```console
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

Jangan gunakan sudo karena akan mengaktifkan nvm untuk user root saja. Script akan mengkloning repositori proyek dari Github ke direktori ``~/.nvm``:

```console
=> Close and reopen your terminal to start using nvm or run the following to use it now:

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" 
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" 
```

Seperti yang dikatakan output di atas, Anda harus menutup dan membuka kembali terminal atau menjalankan perintah untuk menambahkan PATH ke script nvm ke sesi shell saat ini.

Setelah skrip berada di PATH Anda, verifikasi bahwa nvm telah diinstal dengan benar dengan mengetik:

```console
nvm --version
```

```console
Output:
0.35.3
```

Untuk mendapatkan daftar semua versi Node.js yang dapat diinstal dengan nvm, jalankan perintah berikut:

```console
nvm list-remote
```

Perintah tersebut akan menampilkan daftar semua versi Node.js yang tersedia.

Untuk menginstal versi terbaru Node.js yang tersedia, jalankan:

```console
nvm install node
```

Outputnya akan terlihat seperti ini:

```console
...
Checksums matched!
Now using node v14.2.0 (npm v6.14.4)
Creating default alias: default -> node (-> v14.2.0)
```

Setelah instalasi selesai, verifikasi versi Node.js dengan perintah berikut:

```console
node --version
```

```console
Output:
v14.2.0
```

## Penutup

Kita telah belajar tentang 3 cara berbeda untuk menginstal Node.js dan npm di server Ubuntu 20.04 Anda. Metode yang Anda pilih tergantung pada kebutuhan dan preferensi Anda. Meskipun menginstal versi package dari repositori Ubuntu atau NodeSource lebih mudah, metode nvm memberi Anda lebih banyak fleksibilitas untuk menambahkan dan menghapus versi Node.js yang berbeda pada basis per pengguna.
