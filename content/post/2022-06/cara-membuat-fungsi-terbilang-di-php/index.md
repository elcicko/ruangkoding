---
title: Cara Membuat Fungsi Terbilang di PHP
image: "cara-membuat-fungsi-terbilang-di-php.webp"
url: /cara-membuat-fungsi-terbilang-di-php
aliases:
  - /membuat-fungsi-terbilang-di-php/
  - /membuat-fungsi-terbilang-di-php/amp/
description: Pada tutorial ini kita akan membuat sebuah fungsi terbilang di PHP yang digunakan untuk mengkonversi sebuah bilangan atau angka menjadi sebuah kalimat / string.
date: 2022-06-03
author: "Ruang Koding"
lastmod: 2022-06-22
tags: ["PHP"]
categories: ["Pemrograman"]
draft: false
schema:
  image:
    url: "cara-membuat-fungsi-terbilang-di-php.webp"
  author: "Ruang Koding"
  keywords: ["fungsi terbilang di php"]
---

Pada tutorial ini kita akan membuat sebuah fungsi terbilang di PHP yang digunakan untuk mengkonversi sebuah bilangan atau angka menjadi sebuah kalimat / string.

Pertama kita akan menulis sebuah class untuk membuat fungsi terbilang menggunakan PHP. Class ini akan mengkonversi patokan berbentuk angka atau integer jadi sebuah string. Kita akan berikan nama class kita bernama `moneyFormat.php`.

```php
<?php

class moneyFormat {
    
}
```

Setelah itu kita akan membuat method dengan nama `terbilang()`, lalu kita sisipkan sebuah parameter sehingga setiap kita akan memanggil method tersebut, kita hanya perlu menyisipkan parameter berupa angka.

```php
<?php

class moneyFormat {

 public function terbilang($angka) {
            // tulis kode disini
        }
}
```

disini kita buat parameter berupa variable dengan nama `$angka`. setelah itu kita tinggal buat sebuah fungsi untuk memproses angka tersebut untuk membuat fungsi terbilang di PHP.

```php
<?php

class moneyFormat {

 public function terbilang ($angka) {
        // deklarasikan tipe data parameter menjadi float / double
        $angka = (float)$angka;
        $bilangan = ['','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas'];

        if ($angka < 12) {
            return $bilangan[$angka];
        } else if ($angka < 20) {
            return $bilangan[$angka - 10] . ' Belas';
        } else if ($angka < 100) {
            $hasil_bagi = intval($angka / 10);
            $hasil_mod = $angka % 10;
            return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
        } else if ($angka < 200) {
            return sprintf('Seratus %s', $this->terbilang($angka - 100));
        } else if ($angka < 1000) {
            $hasil_bagi = intval($angka / 100);
            $hasil_mod = $angka % 100;
            return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], $this->terbilang($hasil_mod)));
        } else if ($angka < 2000) {
            return trim(sprintf('Seribu %s', $this->terbilang($angka - 1000)));
        } else if ($angka < 1000000) {
            $hasil_bagi = intval($angka / 1000); 
            $hasil_mod = $angka % 1000;
            return sprintf('%s Ribu %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod));
        } else if ($angka < 1000000000) {
            $hasil_bagi = intval($angka / 1000000);
            $hasil_mod = $angka % 1000000;
            return trim(sprintf('%s Juta %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
        } else if ($angka < 1000000000000) {
            $hasil_bagi = intval($angka / 1000000000);
            $hasil_mod = fmod($angka, 1000000000);
            return trim(sprintf('%s Milyar %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
        } else if ($angka < 1000000000000000) {
            $hasil_bagi = $angka / 1000000000000;
            $hasil_mod = fmod($angka, 1000000000000);
            return trim(sprintf('%s Triliun %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
        } else {
            return 'Data Salah';
        }
    }
}
```

Setelah itu kita tinggal membuat object class dan menjalankan function diatas seperti ini :

```php
<?php

$money = new moneyFormat;
$angka = $money->terbilang(50);
echo $angka;
// output : Lima Puluh
```

Sekian dulu tutorial dari [Ruang Koding](<https://ruangkoding.id>) tentang bagaimana cara membuat fungsi terbilang di PHP. Silahkan dicoba dan semoga bermanfaat.
