---
title: Cara Cek Log Apache di Linux
image: "apache-logo.webp"
url: /cara-cek-log-apache-di-linux
aliases:
  - /cara-cek-log-apache-di-linux/amp/
description: Pada tutorial ini kita akan belajar tentang bagaimana cara cek log apache di Linux dengan mudah.
date: 2022-06-13
author: "Ruang Koding"
lastmod: 2022-06-22
tags: ["Apache"]
categories: ["Linux"]
draft: false
schema:
  image:
    url: "apache-logo.webp"
  author: "Ruang Koding"
  keywords: ["cara cek log apache"]
---

Apache adalah bagian dari perangkat lunak LAMP untuk Linux (Linux, Apache, MySQL, PHP). Apache bertanggung jawab untuk menyajikan halaman web kepada visitor yang mengakses situs web Anda.

Server Apache memberikan akses untuk setiap kunjungan ke situs web Anda, dan menyimpan data akses tersebut pada file log. File log ini berisi sumber informasi tentang situs web, penggunaan, dan visitor Anda.

## Cara Cek Log Apache di Linux

Pada tutorial ini kita akan belajar tentang bagaimana cara cek log apache di Linux dan memanfaatkan data log tersebut dengan mudah.

### Lokasi Log Apache di Linux

Pada settingan default, letak file log akses apache di Linux berada pada direktori berikut ini

- **/var/log/apache/access.log**
- **/var/log/apache2/access.log**
- **/etc/httpd/logs/access_log**

Sedangkan untuk log error pada Apache berada pada direktori berikut :

- **/var/log/apache/error.log**
- **/var/log/apache2/error.log**
- **/etc/httpd/logs/error_log**

Anda dapat menjelajai direktori tersebut dengan menggunakan console / terminal dengan perintah ``cd``.

### Menampilkan 100 data terbaru di Log Apache

Untuk dapat menampilkan 100 data yang terbaru di log Apache, Anda bisa menggunakan perintah berikut pada console.

```console
sudo tail -100 /var/log/apache2/access.log
```

Perintah ``tail`` mengintruksikan sistem untuk membaca bagian terakhir dari file, dan parameter ``-100`` digunakan untuk menampilkan 100 data paling terbaru.

Bagian terakhir, ``/var/log/apache2/access.log`` memberi tahu mesin lokasi file log Apache tersebut disimpan. Jika file log Anda berada di direktori yang berbeda, pastikan untuk mengganti parameter ini sesuai dengan lokasi dimana Anda menyimpan file log Apache.

### Menampilkan Kata Kunci Tertentu di Log Apache

Terkadang, Anda hanya ingin menampilkan jenis entri tertentu di log Apache. Anda dapat menggunakan perintah ``grep`` untuk memfilter report menurut kata kunci tertentu.

Misalnya, masukkan perintah berikut ini ke terminal:

```console
sudo grep GET /var/log/apache2/access.log
```

Seperti perintah sebelumnya, parameter ``/var/log/apache2/access.log`` untuk menampilkan isi dari access log Apache. Perintah ``grep`` mengintruksikan mesin untuk hanya menampilkan entri dengan permintaan **GET**.

Anda juga dapat mengganti perintah Apache lainnya. Misalnya, jika Anda ingin memantau akses ke gambar **.jpg**, Anda dapat mengganti **GET** dengan **.jpg**.

## Penutup

Pada artikel ini Kita telah belajar tentang cara cek log Apache di Linux. Analisis **access.log** Apache memudahkan Anda untuk mempelajari cara klien berinteraksi dengan situs web Anda. File **error.log** dapat membantu Anda memecahkan masalah dengan situs web Anda.
