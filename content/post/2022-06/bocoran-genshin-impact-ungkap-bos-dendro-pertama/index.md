---
title: Bocoran Genshin Impact Ungkap Bos Dendro Pertama
image: "genshin-impact-dendro.webp"
url: /bocoran-genshin-impact-ungkap-bos-dendro-pertama
description: Bocoran Genshin Impact yang terbaru mengungkapkan informasi tambahan tentang bos Dendro pertama yang diharapkan hadir di update yang akan datang.
date: 2022-06-10
author: "Ruang Koding"
lastmod: 2022-06-22
tags: ["Genshin Impact"]
categories: ["Game"]
draft: false
schema:
  image:
    url: "genshin-impact-dendro.webp"
  author: "Ruang Koding"
  keywords: ["bocoran genshin impact", "bos dendro genshin impact"]
---

Environment pada Genshin Impact dipenuhi dengan monster menakutkan yang dengan sabar menunggu pertempuran mereka dengan Traveler. Sebagian besar musuh memiliki satu atau lebih elemen dalam kit mereka yang memungkinkan mereka untuk menangani elemental damage.

Dengan rumor baru-baru ini yang mengklaim bahwa penambahan elemen baru sudah dekat, pemain dapat mengharapkan musuh-musuh yang memiliki elemen Dendro kedepannya. Bocoran Genshin Impact baru-baru ini telah mengungkapkan lebih banyak informasi tentang salah satu Bos Wolrd yang akan datang.

Dengan akan berakhirnya seluruh arc Inazuma, para pemain mengharapkan region Genshin Impact yang baru bisa hadir di update yang akan datang. Menurut beberapa bocoran, region baru akan menempatkan elemen Dendro yang telah lama menjadi sorotan. Seorang pembocor yang bernama ***Project Celestia*** telah mengungkapkan lebih banyak detail tentang bos Dendro World pertama pada Genshin Impact. Stage Beta saat ini mencakup beberapa file yang memiliki ***"Jade-Plumed Terrorshroom"*** yang diharapkan akan menjadi nama resmi dari Bos Dendro tersebut. Bos tersebut tampaknya akan menjatuhkan item bernama ***"Nagadus Emerald Gem"*** yang sepertinya akan berfungsi sebagai ascension material karakter yang digunakan untuk menaikkan level karakter Dendro kedepannya.

{{< twitter_simple 1533785777393373184 >}}

Pada salah satu tweetnya, Project Celestia mengklaim bahwa bos Dendro baru mungkin tidak akan menjadi bagian dari update Genshin Impact 2.8 yang akan datang. Dengan asumsi bahwa bocoran sebelumnya tentang 2.8 sebagai update Inazuma terakhir adalah benar, pemain dapat menantikan bos dendro baru tiba di versi 3.0, yang akan dirilis pada akhir Agustus. Namun, rumor baru-baru ini mengungkapkan bahwa durasi 2.7 atau 2.8 akan lebih singkat dari pembaruan biasanya untuk mengkompensasi penundaan update tiga minggu baru-baru ini, yang berarti bahwa 3.0 dapat tiba beberapa minggu lebih awal dari yang diharapkan.

Dendro adalah satu-satunya elemen Genshin Impact yang tidak ditampilkan pada karakter yang dapat dimainkan saat ini. Ada banyak hype seputar kedatangannya, dan itu mungkin tambahan game yang paling dinanti. Kedatangan elemen baru mungkin akan berdampak pada peringkat kekuatan karakter saat ini mengingat hal itu akan memperkenalkan reaksi elemen Dendro tambahan.

Major update lainnya juga akan memperkenalkan region utama baru bernama Sumeru. Beberapa NPC dalam game menggambarkan Sumeru sebagai wilayah misterius yang dipenuhi dengan hutan dan gurun. Berbicara tentang karakter baru Dendro, Baizhu dan Yaoyao dikabarkan akan hadir sejak game tersebut hadir kembali pada tahun 2020. Yaoyao adalah satu-satunya karakter beta asli yang hilang dari roster yang bisa dimainkan. Terlepas dari keduanya, pemain dapat mengharapkan Archon Sumeru, Kusanali, untuk tiba segera setelah 3.0 dirilis.

*Genshin Impact* kini tersedia untuk PC, PS4, PS5, dan perangkat Smartphone. Versi Switch sedang dalam pengembangan tanpa tanggal rilis yang dikonfirmasi.
