---
title: "Cara Generate SSH Key di Ubuntu 20.04"
image: "ubuntu-logo.webp"
url: "/cara-generate-ssh-key-di-ubuntu"
description: Pada artikel kali ini Ruang Koding akan membahas tentang bagaimana cara generate SSH Key di Ubuntu 20.04 untuk dapat mengakses server secara remote.
date: 2022-05-18
lastmod: 2022-06-02
author: "Ruang Koding"
tags: ["Tips & Trik"]
categories: ["Linux"]
draft: false
schema:
  image:
    url: "ubuntu-logo.webp"
  author: "Ruang Koding"
  keywords: ["apa itu ssh","cara generate ssh key di ubuntu"]
---

Pada artikel kali ini [Ruang Koding](https://ruangkoding.id) akan membahas tentang bagaimana [cara generate SSH Key di Ubuntu 20.04](https://ruangkoding.id/cara-membuat-ssh-key-di-ubuntu/) untuk dapat mengakses server secara remote. Hal ini dilakukan agar kita dapat mengakses server tanpa harus menggunakan password. Tentunya dengan menggunakan SSH Key, server kita akan jauh lebih aman.

## Apa itu SSH?

SSH adalah singkatan dari Secure Shell Protocol yaitu merupakan protokol jaringan kriptografi yang memungkinkan pengguna untuk mengakses komputer jarak jauh dengan aman melalui jaringan internet.

Meskipun SSH mendukung otentikasi berbasis kata sandi, umumnya kamu disarankan untuk menggunakan SSH key. SSH key adalah metode yang lebih aman untuk login ke server Linux, karena tidak rentan terhadap serangan peretasan kata sandi brute-force.

Proses generate SSH key di Ubuntu itu sendiri akan menghasilkan dua key yaitu public key dan private key. Kamu dapat menempatkan public key di server mana pun, dan kemudian terhubung ke server menggunakan klien SSH yang memiliki akses ke private key.

Ketika public key dan private key sesuai, server akan memberikan akses tanpa perlu kata sandi. Kamu dapat meningkatkan keamanan SSH Key lebih jauh lagi dengan melindungi private key dengan passphrase, hal ini bersifat opsional tetapi sangat dianjurkan.

## Langkah Cara Membuat SSH Key di Ubuntu

Berikut adalah langkah-langkah yang bisa kamu lakukan untuk membuat SSH key di Ubuntu

### Langkah 1 — Membuat Key Pair

Langkah pertama untuk generate ssh key di Ubuntu adalah membuat Key Pair pada sisi klien atau laptop / PC yang sedang kamu guanakan. Ketik perintah berikut di terminal :

```console
ssh-keygen -t ed25519
```

Perintah diatas akan menghasilkan output sebagai berikut :

```console
Output
Generating public/private ed25519 key pair.
```

Layar konfirmasi bahwa proses generate SSH key telah dimulai lalu Kamu akan dimintai beberapa informasi, yang akan kita bahas pada langkah berikutnya.

### Langkah 2 — Menentukan Tempat Penyimpanan Key Pair

Prompt pertama dari perintah ssh-keygen akan menanyakan di mana kamu harus menyimpan key pair:

```console
Output
Enter file in which to save the key (/home/ruangkoding/.ssh/id_ed25519):
```

Kamu dapat menekan ENTER di sini untuk menyimpan file ke lokasi default di direktori .ssh dari direktori home kamu.

Sebagai alternatif, kamu dapat memilih nama file atau lokasi lain dengan mengetiknya setelah prompt dan menekan ENTER.

### Langkah 3 — Membuat Passphrase

Prompt kedua dan terakhir dari ssh-keygen akan meminta kamu untuk memasukkan passphrase:

```console
Output
Enter passphrase (empty for no passphrase):
```

Kamu bisa membiarkan passphrase kosong, tetapi sangat disarankan untuk mengisi passphrase tersebut agar Key Pair yang kamu buat lebih aman.

Passphrase digunakan agar kamu Key Pair lebih aman digunakan dan tidak akan bisa digunakan oleh sembarang orang karena saat kamu user akan dimintai password saat akan menggunakan key pair tersebut saat ingin mengakses server tujuan.

Output secara keseluruhan adalah sebagai berikut :

```console
Output
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/ruangkoding/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/ruangkoding/.ssh/id_ed25519
Your public key has been saved in /home/ruangkoding/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:EGx5HEXz7EqKigIxHHWKpCZItSj1Dy9Dqc5cYae+1zc ruangkoding@hostname
The key's randomart image is:
+--[ED25519 256]--+
| o+o o.o.++      |
|=oo.+.+.o  +     |
|*+.oB.o.    o    |
|*. + B .   .     |
| o. = o S . .    |
|.+ o o . o .     |
|. + . ... .      |
|.  . o. . E      |
| .. o.   . .     |
+----[SHA256]-----+
```

Public Key sekarang terletak di /home/ruangkoding/.ssh/id_ed25519.pub dan private key sekarang terletak di /home/ruangkoding/.ssh/id_ed25519.

### Langkah 4 — Menyalin Public Key ke Server

Setelah key pair berhasil dibuat, saatnya untuk menempatkan public key di server yang ingin kamu akses.

Kamu dapat menyalin public key ke file authorized_keys yang berada di server tujuan dengan perintah ssh-copy-id. Pastikan untuk mengganti contoh username dan alamat ip sebagai berikut

```console
ssh-copy-id ruangkoding@id_server
```

Dengan perintah tersebut, kamu akan dapat masuk ke server yang ingin kamu akses melalui SSH tanpa perlu menggunakan kata sandi. Namun, jika kamu menetapkan passphrase saat membuat SSH key, kamu akan diminta untuk memasukkan password saat akan login.

## Penutup

Demikian tutorial tentang cara membuat SSH Key di Ubuntu, silahkan kunjungi artikel lainnya tentang bagaimana [cara Login SSH dengan RSA tanpa password](https://ruangkoding.id/tutorial-linux-login-ssh-dengan-pem/). Silahkan dicoba dan semoga bermanfaat.
