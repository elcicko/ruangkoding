---
title: "Spesifikasi Genshin Impact Untuk Android iOS PC Dan PS4"
image: "spesifikasi-genshin-impact-untuk-android-ios-dan-ps4.webp"
url: "/spesifikasi-genshin-impact-untuk-android-ios-dan-ps4"
description: Bagi kamu yang ingin menjajal game ini, kamu bisa cek spesifikasi Genshin Impact untuk berbagai platform dibawah ini.
date: 2022-05-25T22:45:47+07:00
author: "Ruang Koding"
lastmod: 2022-06-02
tags: ["Genshin Impact"]
categories: ["Game"]
draft: false
schema:
  image:
    url: "spesifikasi-genshin-impact-untuk-android-ios-dan-ps4.webp"
  author: "Ruang Koding"
  keywords: ["spesifikasi genshin impact", "spesifikasi genshin impact untuk android"]
---

Sekarang ini masyarakat sedang ramai memperbincangkan tentang Game produksi dari Mihoyo yang bernama Genshin Impact. Permainan RPG open world yang diberi bumbu dengan sistem pertempuran hack and slash tersebut tersedia di bermacam platform dimulai dari PC, PS4, Android, sampai iOS dan memberikan dukungan fitur crossplay.

Genshin Impact sendiri mengangkat design karakter dan environment seperti anime dan dilengkapi dengan kualitas penampilan visual yang memanjakan mata. Game ini juga memerlukan perangkat dengan spesifikasi yang cukup oke supaya bisa lancar dimainkan.

Bagi kamu yang ingin menjajal game ini, kamu bisa cek spesifikasi Genshin Impact untuk berbagai platform dibawah ini lengkap dengan link downloadnya.

## Spesifikasi Genshin Impact Untuk Android, iOS, PC dan PS4

Untuk PC, misalkan, kamu memerlukan pc/laptop dengan spesifikasi paling rendah meliputi processor (CPU) Intel Core i5 atau sepadan, RAM 8 GB, dan pengolah grafis (GPU) Nvidia GeForce GT 1030 ke atas.

Sedangkan untuk versi Android, kamu memerlukan smartphone yang telah dilengkapi dengan chip Snapdragon 845/Kirin 810 atau versi diatasnya, RAM 4 GB, ruangan penyimpanan sampai 8 GB, dan sistem operasi Android 8.1 atau ke atas.

Meskipun begitu, kamu dapat memainkan Genshin Impact di smartphone dengan versi Android 7.0 ke atas serta RAM 3 GB, asal perangkat tersebut telah berbasiskan ARM v8a 64-bit.

Gensin Impact versi iOS hanya memberikan dukungan perangkat iPhone 8 Plus ke atas, iPad Air (Gen. 3) ke atas, iPad mini (Gen. 5), dan iPad Pro (Gen. 2), dengan versi iOS 9.0 ke atas.

Adapun Gensin Impact untuk versi PS4 mendukung semua model PS4 (reguler, Slim, Pro), asal ruang penyimpanannya tersisa kurang lebih 30 GB.

Untuk lebih jelasnya, baca detail lengkap untuk memainkan Genshin Impact, yang sudah dirangkup oleh Ruang Koding dari situs Facebook resmi Genshin Impact.

### Spesifikasi Genshin Impact Untuk PC

**Spesifikasi minimal:**

- Sistem operasi: Windows 7 SP1 64-bit/Windows 8.1 64-bit/Windows 10 64-bit
- Processor: Intel Core i5 atau sepadan
- Memory: RAM 8 GB
- GPU: NVIDIA GeForce GT 1030 ke atas
- Versi DirectX: 11
- Ruang penyimpanan yang dibutuhkan: 30 GB

**Spesifikasi yang disarankan:**

- Sistem operasi: Windows 7 SP1 64-bit/Windows 8.1 64-bit/Windows 10 64-bit
- Processor: Intel Core i7 atau sepadan
- Memory: RAM 16 GB
- GPU: NVIDIA GeForce GTX 1060 6 GB ke atas
- Versi DirectX: 11
- Ruang penyimpanan yang dibutu

Genshin Impact versi PC dapat di download melalui link berikut

[![Download Genshin Impact PC](download-genshin-impact-windows.webp)](https://sg-public-api.hoyoverse.com/event/download_porter/link/ys_global/genshinimpactpc/default)

### Spesifikasi Genshin Impact Untuk Android

**Perangkat yang didukung:**

- Persyaratan kompatibilitas: Perangkat ARM v8a 64-bit
- Memory: RAM 3 GB ataupun lebih
- Sistem operasi yang didukung: Dianjurkan Android 7.0 ke atas
- Ruang penyimpanan yang dibutuhkan: 8 GB
- Perangkat yang dianjurkan:
- CPU: Qualcomm Snapdragon 845, Kirin 810 atau lebih tinggi
- Memory: RAM 4 GB ataupun lebih
- Sistem operasi yang direkomendasikan: Android 8.1 ke atas

Genshin Impact versi Android dapat didownload lewat link berikut lewat smartphone Android yang sudah memenuhi syarat spesifikasi seperti diatas.

[![Download Genshin Impact Android](download-genshin-impact-android.webp)](https://play.google.com/store/apps/details?id=com.miHoYo.GenshinImpact)

### Spesifikasi Genshin Impact Untuk iOS

**Perangkat yang dianjurkan:**

- iPhone: iPhone 8 Plus, iPhone X, iPhone XS, iPhone XS Max, iPhone XR, iPhone 11, iPhone 11 Pro, iPhone 11 Pro Max, iPhone SE (Gen. 2)
- iPad: iPad Air (Gen. 3), iPad mini (Gen. 5), iPad Pro (Gen. 2) 12,9″, iPad Pro (Gen. 2) 10,5″, iPad Pro (Gen. 3) 11″, iPad Pro (Gen. 3) 12,9″, iPad Pro (Gen. 4) 11″, iPad Pro (Gen. 4) 12.9″
- Sistem operasi yang didukung: iOS 9.0 ke atas
- Ruang penyimpanan yang dibutuhkan: 8 GB

Genshin Impact versi iOS dapat didownload lewat link berikut melalui iPhone atau iPad yang sudah memenuhi syarat spesifikasi seperti diatas.

[![Download Genshin Impact iOS](download-genshin-impact-ios.webp)](https://apps.apple.com/app/id1517783697)

### Spesifikasi Genshin Impact Untuk PS4

- PS4 (reguler), PS4 Slim, dan PS4 Pro
- Ruang penyimpanan yang dibutuhkan: 30 GB

Genshin Impact versi PS4 dapat didownload lewat link di bawah ini. kamu juga dapat langsung mendatangi PlayStation Store langsung di perangkat PS4 masing-masing.

[![Download Genshin Impact PS4](download-genshin-impact-ps4.webp)](https://www.playstation.com/games/genshin-impact/)
