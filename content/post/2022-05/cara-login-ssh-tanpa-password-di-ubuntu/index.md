---
title: "Cara Login SSH Tanpa Password di Ubuntu"
image: "ubuntu-logo.webp"
url: "/cara-login-ssh-tanpa-password-di-ubuntu"
description: Pada artikel kali ini kita akan membahas tutorial linux tentang bagaimana cara Login SSH dengan PEM di Ubuntu Server. Dengan cara ini kita dapat melakukan login SSH tanpa password dan hanya mengandalkan file sertifikat PEM saja.
date: 2022-05-19
lastmod: 2022-06-02
author: "Ruang Koding"
tags: ["Tips & Trik"]
categories: ["Linux"]
draft: false
schema:
  image:
    url: "ubuntu-logo.webp"
  author: "Ruang Koding"
  keywords: ["login ssh dengan pem di ubuntu", "login ssh tanpa password"]
---

Pada artikel kali ini kita akan membahas tutorial linux tentang bagaimana cara Login SSH dengan PEM di Ubuntu Server. Dengan cara ini kita dapat melakukan login SSH tanpa password dan hanya mengandalkan file sertifikat PEM saja. Berikut langkah-langkah cara Login SSH di Ubuntu tanpa password dengan menggunakan PEM.

## Login SSH Dengan PEM di Ubuntu Server

Pada tutorial kali ini kita akan menggunakan Ubuntu Server sebagai studi kasus. Kita asumsikan bahwa Kamu sudah menginstall Ubuntu Server dan memiliki hak akses user root di Ubuntu server.

### Langkah 1: Login Ubuntu sebagai user root

Langkah pertama yang harus kamu lakukan adalah login ke Ubuntu server dan masuk sebagai user root.

```console
ubuntu@ubuntu:/# sudo su
```

### Langkah 2: Generate file PEM RSA

Setelah kamu login sebagai root, kamu dapat membuat file pem RSA dengan perintah berikut. Kosongkan passphrase jika kamu diminta untuk mengisi passprashe.

```console
root@ubuntu:/# ssh-keygen -p -m PEM -f ~/.ssh/id_rsa
Key has comment 'root@ubuntu'
Enter new passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved with the new passphrase.
root@ubuntu:/#
```

### Langkah 3: Buka file private key RSA

Jika proses generate file RSA sudah berhasil, kamu dapat membuka file tersebut lalu salin isi file tersebut di komputer lokal kamu.

```console
$cat ~/.ssh/id_rsa
-----BEGIN RSA PRIVATE KEY-----
Mxyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyK
Mxyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyK
Mxyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyz9xyK
NmyVmUKFzZuuq9Dm1I/AVE7hRfIa2Ks5CPumpKDG6wcmRMhbe7jZZKMqAudVKecq
VeM1ub6G5j75jm18lkODDK9ISxGdyiOJoTkx1QDq1syOlWFIFHhrzlL4SI+ipo76
....
....
....
-----END RSA PRIVATE KEY-----
```

### Langkah 4: Disable Password Authentication

Setelah kamu menyalin isi file private key RSA ke sistem lokal kamu, buka file konfigurasi dari SSH untuk mematikan opsi otentikasi dengan password. File konfigurasi SSH berada di direktori `/etc/ssh/sshd_config`, setelah kamu membuat file config SSH cari baris berikut lalu ubah opsi dari yes ke no untuk mengakfitkan login SSH tanpa password.

```console
PasswordAuthentication no
```

Simpan perubahan diatas lalu restart service SSH dengan menggunakan perintah sudo service ssh restart agar sistem Ubuntu dapat menerapkan konfigurasi baru yang sudah kita ubah agar kita dapat login SSH di Ubuntu tanpa password.

### Langkah 5: Login SSH dengan PEM

Jika sistem lokal kamu adalah MacOS atau Linux, ada langkah yang harus kamu lakukan sebelum kita melakukan login ssh dengan PEM di Ubuntu, yaitu dengan mengubah hak akses file PEM dengan perintah chmod.

```console
sudo chmod 600 mykey.pem
```

Setelah kamu mengubah hak akses file pem, maka kamu sudah dapat login SSH dengan PEM dengan menggunakan perintah berikut :

```console
User:~/Keys$ ssh -i mykey.pem ubuntu@IP_ADDRESS
Linux ubuntu 21+ #1399 SMP Thu Jan 28 12:09:48 GMT 2021 armv7l
Build info: Fri Nov 20 09:43:06 UTC 2020 @
Last login: Tue Mar 9 00:23:49 2021 from 192.168.1.73
ubuntu@ubuntu:~ $ 
```

Demikian tutorial dari Ruang Koding tentang bagimana cara login SSH dengan PEM di Ubuntu. Silahkan dicoba dan semoga bermanfaat.
