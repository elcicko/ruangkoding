---
title: "Cara Mengaktifkan GZIP di Nginx untuk Ubuntu Server"
image: "nginx-logo.webp"
url: "/cara-mengaktifkan-gzip-di-nginx-untuk-ubuntu-server"
description: Pada tutorial ini kita akan belajar tentang bagaimana cara mengaktifkan GZIP di Nginx untuk mengkompres konten ataupun data, hal ini akan mengurangi ukuran konten yang dikirim ke pengunjung website sehingga proses penyajian konten menjadi lebih cepat.
date: 2022-05-19
lastmod: 2022-06-02
author: "Ruang Koding"
tags: ["Tips & Trik"]
categories: ["Linux"]
draft: false
schema:
  image:
    url: "nginx-logo.webp"
  author: "Ruang Koding"
  keywords: ["cara mengaktifkan gzip di Nginx", "mengaktifkan kompresi gzip di nginx"]
---

GZIP adalah sebuah jenis kompresi data yang sering digunakan. Pada tutorial ini kita akan belajar tentang bagaimana cara mengaktifkan GZIP di Nginx untuk mengkompres konten ataupun data, hal ini akan mengurangi ukuran konten yang dikirim ke pengunjung website sehingga proses penyajian konten menjadi lebih cepat, dan juga meningkatkan kinerja dari website tersebut.

Untuk dapat mengaktifkan kompresi GZIP di Nginx, Kamu membutuhkan modul dari Nginx yaitu `ngx_http_gzip_module`. Modul ini akan mengkompres semua respons HTTP (file) yang valid menggunakan metode GZIP. Modul ini berguna untuk mengurangi ukuran transfer data dan mempercepat halaman web untuk aset statis seperti JavaScript, file CSS, dan lainnya.

Baca Juga : [Cara Konfigurasi HTTPS Nginx di Ubuntu](https://ruangkoding.id/cara-konfigurasi-https-nginx-di-ubuntu/)

## Langkah-langkah Mengaktifkan GZIP di Nginx

Edit file `/etc/nginx/nginx.conf` atau buat file konfigurasi baru bernama `/etc/nginx/conf.d/static_gzip.conf` :

```console
sudo nano /etc/nginx/nginx.conf
```

Kemudian scroll sampai ke bagian Gzip Settings lalu uncomment sehingga isinya menjadi seperti berikut :

```console
##
# Gzip Settings
##
 
gzip on;
gzip_disable "msie6";

gzip_vary on;
gzip_proxied any;
gzip_comp_level 6;
gzip_buffers 16 8k;
gzip_http_version 1.1;
gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;
# Specify the minimum length of the response to compress (default 20)
gzip_min_length 500;
```

Setelah itu simpan dan tutup file lalu lakukan verifikasi bahwa tidak ada kesalahan dalam file konfigurasi tersebut :

```console
$ nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Jika tidak ada pesan error, maka kita bisa mengaktifkan GZIP di Nginx dengan cara merestart nginx dengan perintah berikut :

```console
sudo service nginx reload
```

atau dengan perintah

```console
sudo systemctl reload nginx
```

ataupun juga dengan perintah berikut :

```console
sudo /etc/init.d/nginx reload
```

Setelah itu kita dapat mengecek apakah aktivasi GZIP di Nginx sudah berhasil dengan perintah curl :

```console
curl -I -H 'Accept-Encoding: gzip,deflate' https://nama-domain-kamu/file.css
```

Kemudian hasilnya adalah sebagai berikut :

```console
HTTP/1.1 200 OK
Server: nginx
Date: Sun, 05 Mar 2017 18:45:31 GMT
Content-Type: text/html; charset=UTF-8
Connection: keep-alive
Vary: Accept-Encoding
X-Whom: l1-com-cyber
Strict-Transport-Security: max-age=15768000; includeSubdomains
Link: ; rel="https://api.w.org/"
X-Varnish: 1812270 1794298
Age: 475
Via: 1.1 varnish-v4
Front-End-Https: on
Content-Encoding: gzip
```

Jika terdapat baris seperti **Content-Encoding: gzip** berarti proses aktivasi GZIP di Nginx sudah berhasil. Silahkan dicoba dan semoga bermanfaat.
