---
title: Tentang Kami
description: Ruang Koding berdedikasi untuk menyajikan artikel dan juga informasi seputar teknologi baik itu pemrograman dan juga tentang game.
date: '2019-02-28'
aliases:
  - about-us
  - contact
lastmod: '2022-05-19'
menu:
    main: 
        weight: -90
        params:
            icon: user
---

Selamat datang di Ruang Koding, Kami berdedikasi untuk untuk menyajikan artikel dan juga informasi seputar teknologi baik itu pemrograman dan juga tentang game.

Kami harap Anda menikmati artikel kami sama seperti kami menikmati menawarkannya kepada Anda. Jika Anda memiliki pertanyaan atau komentar, jangan ragu untuk menghubungi kami.
