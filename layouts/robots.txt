User-agent: *
Allow: *.js
Allow: *.css
Disallow: /?keyword=
{{ range where .Data.Pages "Params.robotsdisallow" true }}
Disallow: {{ .RelPermalink }}
{{ end -}}
Sitemap: {{ "sitemap.xml" | absLangURL }}